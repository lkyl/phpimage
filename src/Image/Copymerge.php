<?php
namespace Phpmedia\Image;

use Phpmedia\Image\ActionAbstract;

/*
 ******************************合成图片
 */
class Copymerge extends ActionAbstract
{
    public function __construct()
    {

    }

    //目标图片透明的地方不透明了，也就是说png透明的会被当做白色处理
    //imagecopymerge ( resource $dst_im , resource $src_im , int $dst_x , int $dst_y , int $src_x , int $src_y , int $src_w , int $src_h , int $pct )---拷贝并合并图像的一部分
    //将 src_im 图像中坐标从 src_x，src_y 开始，宽度为 src_w，高度为 src_h 的一部分拷贝到 dst_im 图像中坐标为 dst_x 和 dst_y 的位置上。两图像将根据 pct 来决定合并程度，其值范围从 0 到 100。当 pct = 0 时，实际上什么也没做，当为 100 时对于调色板图像本函数和 imagecopy() 完全一样，它对真彩色图像实现了 alpha 透明
    public function doMerge($dis, $src, $newImg)
    {
        $imageD = imagecreatefrompng($dis);//目标图
        $imageS = imagecreatefrompng($src);
        
        imagecopymerge($imageD, $imageS, 0, 0, 0,0, imagesx($imageS), imagesy($imageS), 0);;
        imagepng($imageD, $newImg);//bool(true)
    }

    //透明的部分依然是透明的
    public function doCopy($dis, $src, $newImg, $width = 0, $height = 0)
    {
        $imageSize = getimagesize($dis);
        if ($imageSize[2] === 2) {
            $imageD = imagecreatefromjpeg($dis);//目标图jpg
        } elseif ($imageSize[2] === 3) {
            $imageD = imagecreatefrompng($dis);//目标图png
        } else {
            return false;
        }

        $imageS = imagecreatefrompng($src);
        $width = $width ? : imagesx($imageS);
        $height = $height ? : imagesy($imageS);
        
        imagecopy($imageD, $imageS, 0, 0, 0,0, $width, $height);
        return imagepng($imageD, $newImg);//bool(true)
    }
}
