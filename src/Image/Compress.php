<?php
namespace Phpmedia\Image;

use Phpmedia\Image\ActionAbstract;

/*
 ******************************压缩图片
 */
class Compress extends ActionAbstract
{
    public function __construct()
    {

    }

    //源文件,新的宽，新的高
    //return 新文件的地址
    public function zoomImg($file, $nw, $nh)
    {
        $nfile = '/tmp/shuiyin/zoom.png';
        $new = imagecreatetruecolor($nw, $nh);
        list($width, $height) = getimagesize($file);
        $img = imagecreatefromjpeg($file);

        imagecopyresized($new, $img,0, 0,0, 0, $nw, $nh, $width, $height);

        imagejpeg($new, $nfile);
        imagedestroy($new);
        imagedestroy($img);
        return $nfile;
    }
}
