<?php
namespace Phpmedia\Font;

use Phpmedia\Font\ActionAbstract;

/*
 **************************文字写入图片
 */
class WordsOnImg extends ActionAbstract
{
    public $config = null;

    //$config['file'] 图片文件
    //$config['size'] 文字大小
    //$config['angle'] 文字的水平角度
    //$config['fontfile'] 字体文件路径
    //$config['width'] 预先设置的宽度
    //$config['x'] 开始写入时的横坐标
    //$config['y'] 开始写入时的纵坐标
    //$config['nFile'] 新图。如果不浏览器直接输出的话
    public function __construct($config=null)
    {
        if(empty($config)){
            echo 'must be config';exit;
        }
        $this->config = $config;
    }

    /**
     * PHP实现图片上写入实现文字自动换行
     * @param $fontsize 字体大小
     * @param $angle 角度
     * @param $font 字体路径
     * @param $string 要写在图片上的文字
     * @param $width 预先设置图片上文字的宽度
     * @param $flag  换行时单词不折行
     */
    public function wordWrap($fontsize,$angle,$font,$string,$width,$flag=true) {
        $content = "";
        if($flag){
            $words = explode(" ",$string);
            foreach ($words as $key=>$value) {
                $teststr = $content." ".$value;
                $testbox = imagettfbbox($fontsize, $angle, $font, $teststr);
                if(($testbox[2] > $width)) {
                    $content .= "\n";
                }
                $content .= $value." ";
            }
        } else {
            for ($i=0;$i<mb_strlen($string);$i++) {
                $letter[] = mb_substr($string, $i, 1);
            }
            foreach ($letter as $l) {
                $teststr = $content." ".$l;
                $testbox = imagettfbbox($fontsize, $angle, $font, $teststr);
                // 判断拼接后的字符串是否超过预设的宽度
                if (($testbox[2] > $width) && ($content !== "")) {
                    $content .= "\n";
                }
                $content .= $l;
            }

        }
        return $content;
    }

    /**
     * 实现写入图片
     * @param $text 要写入的文字
     * @param $flag 是否直接输出到浏览器，默认是
     */
    public function writeWordsToImg($text,$flag=true){
        if(empty($this->config)){
            echo "must be config";exit;
        }
        $img_pathWH = getimagesize($this->config['file']);
        $im = imagecreatefrompng($this->config['file']);
        #设置水印字体颜色
        $color = imagecolorallocatealpha($im, 255, 255, 255, 0);//0-127不透明到透明
        $have = false;
        if(stripos($text,"<br/>") !== false){
            $have = true;
        }
        if($have){
            $words_text = explode("<br/>",$text);
            $words_text[0] = $this->wordWrap($this->config['size'], $this->config['angle'], $this->config['fontfile'], $words_text[0], $this->config['width']); //自动换行处理
            $words_text[1] = $this->wordWrap($this->config['size'], $this->config['angle'], $this->config['fontfile'], $words_text[1], $this->config['width']); //自动换行处理
            $words_text[2] = $this->wordWrap($this->config['size'], $this->config['angle'], $this->config['fontfile'], $words_text[2], $this->config['width']); //自动换行处理
            imagettftext($im, $this->config['size'], $this->config['angle'], $this->config['x'], $this->config['y'], $color, $this->config['fontfile'], $words_text[0]);
            imagettftext($im, $this->config['size'], $this->config['angle'], $this->config['x'], $this->config['y']+30, $color, $this->config['fontfile'], "  ".$words_text[1]);
            imagettftext($im, $this->config['size'], $this->config['angle'], $img_pathWH[0]/2+70, $img_pathWH[1]-80, $color, $this->config['fontfile'], $words_text[2]);
            if($flag){
                header("content-type:image/png");
                imagepng($im);
                imagedestroy($im);
            } 
            imagepng($im,$this->config['file_name'].'_1.'.$this->config['file_ext']);
            imagedestroy($im);
        }
        $words_text = $this->wordWrap($this->config['size'], $this->config['angle'], $this->config['fontfile'], $text, $this->config['width']); //自动换行处理
        imagettftext($im, $this->config['size'], $this->config['angle'], $this->config['x'], $this->config['y'], $color, $this->config['fontfile'], $words_text);
        if($flag){
            header("content-type:image/png");
            imagepng($im);
            imagedestroy($im);
        }
        imagepng($im, $this->config['nFile']);
        imagedestroy($im);
    }
}
